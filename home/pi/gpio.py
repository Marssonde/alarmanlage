#!/usr/bin/env python

# Public Domain

import sys
import time
import pigpio
import datetime
import subprocess

stopp = 0  # Zeitpunkt steigende Flanke 
start = 0  # Zeitpunkt fallende Flanke
delta = 0  # Zeitdifferenz zwischen start und stopp
pausemode=0
GPIOreedcontact=22
GPIOshutdown=16
GPIOrot=23
GPIOgelb=24
GPIOgruen=25
GLITCH=400
bm=26
pi = pigpio.pi()       # pi accesses the local Pi's GPIO

if not pi.connected:
   exit(0)

pi.set_mode(GPIOreedcontact, pigpio.INPUT)
pi.set_glitch_filter(GPIOreedcontact, GLITCH)

pi.set_mode(GPIOshutdown, pigpio.INPUT)
pi.set_glitch_filter(GPIOshutdown, GLITCH)

pi.set_mode(GPIOrot, pigpio.OUTPUT)
pi.write(GPIOrot, 0) # set red LED on

pi.set_mode(GPIOgelb, pigpio.OUTPUT)
pi.write(GPIOgelb, 0) # set yellow LED on

pi.set_mode(GPIOgruen, pigpio.OUTPUT)
pi.write(GPIOgruen, 0) # set green LED on

pi.set_mode(bm, pigpio.INPUT)

time.sleep(2)
pi.write(GPIOrot, 1) # set red LED off

pi.write(GPIOgelb, 1) # set yellow LED off

pi.write(GPIOgruen, 0) # set green LED on

def cbf1(gpio, level, tick):
   global start
   global stopp
   global delta
   global GPIOrot
   global pausemode
   if pausemode == 0:
      # fallende Flanke, Startzeit speichern
      if level == 1:
         start = time.time()
         print("------------------------------")
         print(" ")
         pi.write(GPIOrot, 0) # set yellow LED on
         print("Door is open")
         print(time.strftime("%d.%m.%Y %H:%M:%S"))
         print("------------------------------")
         subprocess.call(["/home/pi/door-status.sh","0"])
      if level == 0:
         # steigende Flanke, Endezeit speichern
         stopp = time.time()
         delta = stopp - start       # Zeitdifferenz berechnen
         print("------------------------------")
         print(" ")
         pi.write(GPIOrot, 1) # set yellow LED off
         print("Door is closed = %1.2f" % delta)
         print(time.strftime("%d.%m.%Y %H:%M:%S"))
         print("------------------------------")
         subprocess.call(["/home/pi/door-status.sh","1"])

def cbf3(gpio, level, tick):
   global bm
   global GPIOgelb
   global start
   global stopp
   global pausemode
   if pausemode==0:
      pi.write(GPIOgelb, 0) # set yellow LED on
      print("Bewegung erkannt")
      print("Door:")
      start = time.time()
      print("")
      print(time.strftime("%d.%m.%Y %H:%M:%S"))
      print("------------------------------")
      subprocess.call(["/home/pi/door-status.sh","2"])

def cbf4(gpio, level, tick):
   global bm
   global GPIOgelb
   global start
   global stopp
   global pausemode
   if pausemode==0:
      pi.write(GPIOgelb, 1) # set yellow LED off
      print("Bewegung beendet")
      print("Door:")
      # steigende Flanke, Endezeit speichern
      stopp = time.time()
      delta = stopp - start       # Zeitdifferenz berechnen
      print("Bewegungszeit = %1.2f" % delta)
      print(time.strftime("%d.%m.%Y %H:%M:%S"))
      print("------------------------------")
      subprocess.call(["/home/pi/door-status.sh","3"])

def cbf6(gpio, level, tick):
   global GPIOrot
   global GPIOgelb
   global GPIOgruen
   global pausemode
   global start
   global stopp
   global delta
   if level==0:
         start = time.time()
         stopp=0
         delta=0
   if level==1:
         stopp = time.time()
         delta = stopp - start  # Zeitdifferenz berechnen
         print("motion button pressed:")
         print(time.strftime("%d.%m.%Y %H:%M:%S"))
         print("------------------------------")
         if delta>3:
              pi.write(GPIOrot, 0) # led on
              pi.write(GPIOgruen, 0) # led on
              pi.write(GPIOgelb, 0) # led on 
              time.sleep(0.5)
              pi.write(GPIOrot, 1) # led off
              pi.write(GPIOgruen, 1) # led off
              pi.write(GPIOgelb, 1) # led on 
              time.sleep(0.5)
              pi.write(GPIOrot, 0) # led on
              pi.write(GPIOgruen, 0) # led on
              pi.write(GPIOgelb, 0) # led on 
              time.sleep(0.5)
              pi.write(GPIOrot, 1) # led off
              pi.write(GPIOgruen, 1) # led off
              pi.write(GPIOgelb, 1) # led on 
              time.sleep(0.5)
              pi.write(GPIOrot, 0) # led on 
              pi.write(GPIOgruen, 0) # led on 
              pi.write(GPIOgelb, 0) # led on 
              subprocess.call(["/home/pi/door-status.sh","8"])
         else:
              if pausemode==0:
                  pausemode=1
                  pi.write(GPIOrot, 0) # set red LED on
                  pi.write(GPIOgelb, 1)  # set yellow LED off
                  pi.write(GPIOgruen, 1) # set green LED off
                  subprocess.call(["/home/pi/door-status.sh","4"])
              else:
                  pausemode=0
                  pi.write(GPIOrot, 1) # set red LED off
                  pi.write(GPIOgelb, 1) # set yellow LED off
                  pi.write(GPIOgruen, 0) # set green LED on
                  subprocess.call(["/home/pi/door-status.sh","5"])

try:
   cb1 = pi.callback(GPIOreedcontact, pigpio.EITHER_EDGE, cbf1)
   cb3 = pi.callback(bm, pigpio.RISING_EDGE, cbf3)
   cb4 = pi.callback(bm, pigpio.FALLING_EDGE, cbf4)
   cb6 = pi.callback(GPIOshutdown, pigpio.EITHER_EDGE, cbf6)
   # Endlosschleife, bis Strg-C gedrueckt wird
   while True:
     # nix Sinnvolles tun
     time.sleep(100)

except KeyboardInterrupt:
   print("\nBye")
   cb1.cancel()
   cb3.cancel()
   cb4.cancel()
   cb6.cancel()
   exit()
