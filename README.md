# Alarmanlage
Alarmanlage mit RaspberryPi und Kamera, Bewegungsmelder und Reed Koontakt.

![Alarmanlage](https://github.com/mars7105/alarmanlage/blob/master/bild.jpg "Alarmanlage")
# (Die Links und der Anbieter dienen nur zur Orientierung)
# 1. Benötigte Hardware: 
Name  |  Link  |  Bild
----------  |  ----------  |  ---------- 
RspberryPi 3B+  |  https://www.reichelt.de/raspberry-pi-3-b-4x-1-4-ghz-1-gb-ram-wlan-bt-raspberry-pi-3b-p217696.html  |  ![RspberryPi](https://cdn-reichelt.de/bilder/web/artikel_ws/A300/RASPBERRY_PI_3B_PLUS_001.jpg "RspberryPi")
Kamera mit IR für Nachtsicht |   https://www.reichelt.de/raspberry-pi-kamera-5mp-ir-1080p-ov5647-rpi-jt-can-5mp-p219879.html?&trstct=pos_1  |  ![Kamera](https://cdn-reichelt.de/bilder/web/xxl_ws/A300/RPI_JT_CAN_5MP_01.png "Kamera")
Gehäuse für RspberryPi 3B+ ( am besten mit Lüfter) | https://www.reichelt.de/index.html?ACTION=446&LA=446&q=raspberrypi%203b%2B%20geh%C3%A4use |  ![Gehäuse](https://cdn-reichelt.de/resize_150x150/web/artikel_ws/C700/RPI_CASE_ALU_BK_01.jpg "Gehäuse")
MicroSDHC | https://www.reichelt.de/microsdhc-speicherkarte-32gb-samsung-evo-plus-sams-mb-mc32ga-p207608.html?&trstct=lsbght_sldr::164977 |  ![MicroSDHC](https://cdn-reichelt.de/bilder/web/artikel_ws/E910/SAMSUNG_MB-MC32GAEU_01.jpg "MicroSDHC")
Raspberry Pi - Netzteil | https://www.reichelt.de/raspberry-pi-netzteil-5-v-2-5-a-micro-usb-schwarz-rasp-nt-25-sw-e-p240934.html?&trstct=lsbght_sldr::164977 |  ![Netzteil](https://cdn-reichelt.de/bilder/web/artikel_ws/D400/RASPNT25SWE.jpg "Netzteil")
Gehäuse für Schaltung | https://www.reichelt.de/index.html?ACTION=446;GROUPID=7723;SEARCH=geh%C3%A4use |    ![Gehäuse](https://cdn-reichelt.de/resize_150x150/web/artikel_ws/C700/ALUFORM.jpg "Gehäuse")
Kabel für Schaltung | https://www.reichelt.de/entwicklerboards-steckbrueckenkabel-20-pole-m-m-f-f-f-m-25-debo-kabelset-p161046.html?&trstct=pos_2 |    ![Kabel](https://cdn-reichelt.de/bilder/web/artikel_ws/A300/KABELFRRASPBERRY.jpg "Kabel")
1 bis 2 Platinen | https://www.reichelt.de/Streifenraster/2/index.html?ACTION=2&LA=2&GROUPID=7786 |    ![Platinen](https://cdn-reichelt.de/resize_150x150/web/artikel_ws/C900/H25PS160.jpg "Platinen")
***
# 2. Elektronische Bauteile:
ich persönlich habe gleich mehrere Bauteile gekauft, für den Fall das ein Bauteil verloren oder kaputt geht.

## LEDS:
Name  |  Link  |  Foto
----------  |  ----------  |  ----------
1 LED 5mm gelb Low Currency  |  https://www.reichelt.de/led-5-mm-bedrahtet-gelb-1-2-mcd-50-vis-tlly-5400-p231004.html?&trstct=pol_15  |  ![gelb](https://cdn-reichelt.de/bilder/web/artikel_ws/A500/A500_TLLY.jpg "gelb")
1 LED 5mm grün Low Currency  |  https://www.reichelt.de/led-5-mm-bedrahtet-gruen-1-2-mcd-50-vis-tllg-5400-p231002.html?&trstct=pol_14  |  ![grün](https://cdn-reichelt.de/bilder/web/artikel_ws/A500/TLLG.jpg "grün")
1 LED 5mm rot Low Currency  |  https://www.reichelt.de/led-5-mm-bedrahtet-rot-1-2-mcd-50-vis-tllr-5400-p231003.html?&trstct=pol_2  |  ![rot](https://cdn-reichelt.de/bilder/web/artikel_ws/A500/A500_TLLR.jpg "rot")

Du kannst natürlich auch andere LED's nehmen. 3 Fassungen für die LED's sind sicherlich sehr nützlich.

## Widerstände:
Eventuell andere Vorwiderstände für die LED's wählen. Die GPIO Ports haben eine Ausgangsspannung von 3,3 Volt. Ich habe LED's mit 2mA gewählt, um die Ausgänge des Raspby nicht zu sehr zu belasten. Hier kannst Du die Widerstände berechnen:
https://www.spaceflakes.de/led-rechner/

Name  |  Link  |  Foto
----------  |  ----------  |  ----------
1x 845 Ohm (Wert kann je nach LED variieren)  |  https://www.reichelt.de/widerstand-metallschicht-845-ohm-0207-0-6-w-1-metall-845-p12008.html?&trstct=pos_0  |  ![Widerstände](https://cdn-reichelt.de/bilder/web/artikel_ws/B400/!WIDMET.jpg "Widerstände")
2x 680 Ohm (Wert kann je nach LED variieren)  |  https://www.reichelt.de/widerstand-metallschicht-680-ohm-0207-0-6-w-1-metall-680-p11942.html?&trstct=lsbght_sldr::12008  |  ![Widerstände](https://cdn-reichelt.de/bilder/web/artikel_ws/B400/!WIDMET.jpg "Widerstände")
2x 1k Ohm  |  https://www.reichelt.de/widerstand-metallschicht-1-00-kohm-0207-0-6-w-1-metall-1-00k-p11403.html?&trstct=lsbght_sldr::11449  |  ![Widerstände](https://cdn-reichelt.de/bilder/web/artikel_ws/B400/!WIDMET.jpg "Widerstände")
2x 10k Ohm  |  https://www.reichelt.de/widerstand-metallschicht-10-0-kohm-0207-0-6-w-1-metall-10-0k-p11449.html?&trstct=lsbght_sldr::12008  |  ![Widerstände](https://cdn-reichelt.de/bilder/web/artikel_ws/B400/!WIDMET.jpg "Widerstände")

## Kondensator:

Name  |  Link  |  Foto
----------  |  ----------  |  ----------
1x 100nF  |  https://www.reichelt.de/folienkondensator-100nf-63v-rm5-mks2-63-100n-p12349.html?&trstct=pos_1  |  ![Kondensator](https://cdn-reichelt.de/bilder/web/artikel_ws/B300/MKS2.jpg "Kondensator")

## Bewegungssensor:
Name  |  Link  |  Foto
----------  |  ----------  |  ----------
Infrarot Bewegungsmelder, HC-SR501  |  https://www.reichelt.de/raspberry-pi-infrarot-bewegungsmelder-hc-sr501-rpi-hc-sr501-p224216.html?&trstct=lsbght_sldr::266046  |  ![Bewegungssensor](https://cdn-reichelt.de/bilder/web/artikel_ws/A300/HC-SR501_1.jpg "Bewegungssensor")

## Reedschalter (Magnetschalter)
Name  |  Link  |  Foto
----------  |  ----------  |  ----------
Reedschalter  |  https://www.reichelt.de/reed-sensor-175-v-0-5-a-wechsler-mk26-1c90c-p151871.html?&trstct=lsbght_sldr::151829  |  ![Reedschalter](https://cdn-reichelt.de/bilder/web/artikel_ws/C300/MK26_PAIR.jpg "Reedschalter")
Magnet  |  https://www.reichelt.de/magnet-32mm-x-16mm-x-10mm-magnet-02-p151641.html?&trstct=lsbght_sldr::151871  |  ![Magnet](https://cdn-reichelt.de/bilder/web/artikel_ws/C300/MAGNET_M05.jpg "Magnet")

***
# 3. Werkzeuge:
* Lötkolben
* Schraubendreher
* Bohrmaschine
* Bohrer ca. 3mm bis 8mm

***
# 4. Verbrauchsmaterial:
* Lötzinn
* Silberdraht
* Schrauben mit Muttern, ca. 3mm bis 6mm
* Schrumpfschläuche

***
# 5. Schaltplan:
![Schaltplan](https://github.com/mars7105/alarmanlage/blob/master/schaltplan.png "Schaltplan")

***
# 6. Software für den Raspby:
Name  |  Link  |  Howto
----------  |  ----------  |  ----------
Raspbian Buster Lite  |  https://www.raspberrypi.org/downloads/raspbian/  |  -
motion  |  https://motion-project.github.io/  |  -
motioneyes  |  https://github.com/ccrisan/motioneye/wiki  |  -
Python  |  https://www.python.org/  |  https://wiki.ubuntuusers.de/Python/
pigpio  |  http://abyz.me.uk/rpi/pigpio/  |  -
Gnupg  |  https://gnupg.org/  |  https://wiki.ubuntuusers.de/GnuPG/
sendemail  |  https://packages.debian.org/de/buster/sendemail  |  https://packages.debian.org/buster/sendemail

***

# 7. Software für den PC:
Name  |  Link
----------  |  ----------
Fritzing (Schaltplanedtor)  |  https://fritzing.org/



